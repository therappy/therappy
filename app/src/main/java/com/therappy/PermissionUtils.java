package com.therappy;

import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.util.Log;

public class PermissionUtils {

    private static String TAG = PermissionUtils.class.getName();

    /**
     * this methods checks grant result. i.e whether user pressed allow or deny.
     * Must be called in  onRequestPermissionsResult()
     *
     * @param grantResults grant results got in onRequestPermissionsResult
     * @return true if user pressed allow for all permissions, false otherwise
     */
    public boolean checkGrantResults(@NonNull int[] grantResults) {
        boolean isGranted = true;
        for (int grantResult : grantResults) { //iterate though grant results for asked permissions
            if (grantResult != PackageManager.PERMISSION_GRANTED) { //if any asked permission is not granted
                isGranted = false;
                break;
            }
        }
        Log.i(TAG, "checkGrantResults() returned: " + isGranted);
        return isGranted;
    }
}
