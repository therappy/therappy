package com.therappy;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.therappy.main.ItemData;

import java.util.ArrayList;
import java.util.List;

public class UserOverviewAdapter extends RecyclerView.Adapter<UserOverviewAdapter.ItemViewHolder> {

    private List<ItemData> data = new ArrayList<>();

    public UserOverviewAdapter() {
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View row = inflater.inflate(R.layout.data_item, null);
        return new ItemViewHolder(row);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        final ItemData item = data.get(position);

        holder.description.setText(item.getDescription());

        Glide.with(holder.itemView)
                .load(item.getImagePath())
                .into(holder.image);

        Glide.with(holder.itemView)
                .load(item.getIcon())
                .into(holder.icon);

    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }

    public void setData(final List<ItemData> data) {
        this.data = new ArrayList<>();
        this.data = data;

        notifyDataSetChanged();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private ImageView image;
        private TextView description;
        private ImageView icon;

        ItemViewHolder(View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.image);
            description = itemView.findViewById(R.id.description);
            icon = itemView.findViewById(R.id.icon);
        }
    }
}
