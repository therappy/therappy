package com.therappy.main;

import android.Manifest;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.therappy.EditProfileActivity;
import com.therappy.PermissionUtils;
import com.therappy.R;

import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;

public class EditProfileFragment extends Fragment {
    public static final int CAMERA_PERMISSIONS_REQUEST_CODE = 100;
    public static final int GALLERY_PERMISSIONS_REQUEST_CODE = 101;

    private static final int PICK_GALLERY_REQUEST_CODE = 102;
    private static final int PICK_CAMERA_REQUEST_CODE = 103;

    public PermissionUtils mPermissionUtils = new PermissionUtils();

    private CircleImageView userImageProfile;

    private Uri takenPicturePath;

    protected View mainView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mainView = inflater.inflate(R.layout.fragment_edit_profile, container, false);

        userImageProfile = mainView.findViewById(R.id.iv_user_profile_image);
        userImageProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onUserProfileClicked();
            }
        });

        return mainView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_GALLERY_REQUEST_CODE) {
            if (data != null && data.getData() != null) {
                setUserProfileImage(data.getData());
            }
        } else if (requestCode == PICK_CAMERA_REQUEST_CODE) {
            setUserProfileImage(takenPicturePath);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case GALLERY_PERMISSIONS_REQUEST_CODE:
                if (mPermissionUtils.checkGrantResults(grantResults)) {
                    openGallery();
                } else {
                    showToast(getString(R.string.activity_edit_profile_gallery_permission_is_required));
                }
                break;
            case CAMERA_PERMISSIONS_REQUEST_CODE:
                if (mPermissionUtils.checkGrantResults(grantResults)) {
                    openCamera();
                } else {
                    showToast(getString(R.string.activity_edit_profile_camera_permission_is_required));
                }
                break;
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_done) {
            //TODO: save profile
            Intent intent = new Intent(getContext(), EditProfileFragment.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        return true;
    }

    private void setUserProfileImage(final Uri uri) {
        Glide.with(getContext())
                .load(uri)
                .into(userImageProfile);
    }

    private void onUserProfileClicked() {
        //Si quieres ponerle tu diseño propio al alertDialog
        //android.app.AlertDialog.Builder pictureDialog = new android.app.AlertDialog.Builder(this, R.style.my_custom_theme);
        android.app.AlertDialog.Builder pictureDialog = new android.app.AlertDialog.Builder(getActivity());
        pictureDialog.setTitle(getString(R.string.activity_edit_select_action));
        String[] pictureDialogItems = {
                getString(R.string.activity_edit_camera),
                getString(R.string.activity_edit_gallery)};
        pictureDialog.setItems(pictureDialogItems,
                (dialog, which) ->
                {
                    switch (which) {
                        case 0:
                            takePhotoFromCamera();
                            dialog.dismiss();
                            break;
                        case 1:
                            chooseImageFromGallery();
                            dialog.dismiss();
                            break;
                    }
                });

        pictureDialog.show();
    }

    private void chooseImageFromGallery() {
        int permissionCheck = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, GALLERY_PERMISSIONS_REQUEST_CODE);
        } else {
            openGallery();
        }
    }

    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        intent.setType("image/*");

        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(intent, PICK_GALLERY_REQUEST_CODE);
        }
    }

    private void takePhotoFromCamera() {
        int permissionCameraCheck = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA);
        int permissionWriteCheck = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCameraCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, CAMERA_PERMISSIONS_REQUEST_CODE);
        } else if (permissionWriteCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, CAMERA_PERMISSIONS_REQUEST_CODE);
        } else {
            openCamera();
        }
    }

    private void openCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        addExtraUri(intent);

        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(intent, PICK_CAMERA_REQUEST_CODE);
        }
    }

    private void addExtraUri(final Intent intent) {
        String fileName;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            ContentValues values = new ContentValues();
            //TODO: change proper file name
            fileName = getFileName();
            values.put(MediaStore.Images.Media.TITLE, fileName);
            takenPicturePath = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

            intent.putExtra(MediaStore.EXTRA_OUTPUT, takenPicturePath);
        } else {
            File file;
            fileName = getFileName();
            if (fileName != null) {
                file = new File(fileName);
                // generated file name!!
                takenPicturePath = Uri.fromFile(file);
            }
        }

        Log.d(EditProfileActivity.class.getName(), takenPicturePath.getPath());
    }

    @Nullable
    private String getFileName() {
        File externalFilesDir = getActivity().getExternalCacheDir();
        if (externalFilesDir == null) return null;
        String path = externalFilesDir.getPath();

        //TODO: change proper image name
        return path + "/" + String.valueOf(System.currentTimeMillis()) + "_therappy" + ".jpg";
    }

    protected void showToast(final String text) {
        if (text != null && !text.isEmpty()) {
            Toast.makeText(getContext(), text, Toast.LENGTH_SHORT).show();
        }
    }
}
