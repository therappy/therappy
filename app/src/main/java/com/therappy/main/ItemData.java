package com.therappy.main;

public class ItemData {

    private String imagePath;
    private String description;
    private int icon;

    public ItemData(String imagePath, String description, int icon) {
        this.imagePath = imagePath;
        this.description = description;
        this.icon = icon;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}
