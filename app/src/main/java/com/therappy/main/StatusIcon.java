package com.therappy.main;

import android.support.annotation.DrawableRes;

import com.therappy.R;

public enum StatusIcon {

    DISGUSTED(0, R.mipmap.ic_disgusted),
    FEARLESS(1, R.mipmap.ic_fearless),
    HAPPY(2, R.mipmap.ic_happy),
    MAD(3, R.mipmap.ic_mad),
    SAD(4, R.mipmap.ic_sad);

    private final int status;

    @DrawableRes
    private final int iconRes;

    StatusIcon(final int status, final @DrawableRes int iconRes) {
        this.status = status;
        this.iconRes = iconRes;
    }

    @DrawableRes
    public int getIconRes(final int status) {
        return iconRes;
    }
}
