package com.therappy.main;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.therappy.R;
import com.therappy.UserOverviewAdapter;

import java.util.ArrayList;
import java.util.List;

public class UserOverviewFragment extends Fragment {

    private View mainView;
    private UserOverviewAdapter adapter;
    private RecyclerView recyclerView;
    private View emptyView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mainView = inflater.inflate(R.layout.fragment_user_overview, container, false);
        recyclerView = mainView.findViewById(R.id.recycler_view);

        emptyView = mainView.findViewById(R.id.empty_view);

        adapter = new UserOverviewAdapter();
        recyclerView.setAdapter(adapter);

        return mainView;
    }

    @Override
    public void onResume() {
        super.onResume();

        initView();
    }

    private void initView() {
        //aqui tendrias q traerte tus datos de firebase
        ItemData item1 = new ItemData("", "Item data 1 description", R.mipmap.ic_sad);
        ItemData item2 = new ItemData("", "Item data 2 description", R.mipmap.ic_mad);
        ItemData item3 = new ItemData("", "Item data 3 description", R.mipmap.ic_fearless);
        ItemData item4 = new ItemData("", "Item data 4 description", R.mipmap.ic_happy);
        ItemData item5 = new ItemData("", "Item data 5 description", R.mipmap.ic_disgusted);
        List<ItemData> data = new ArrayList<>();
        data.add(item1);
        data.add(item2);
        data.add(item3);
        data.add(item4);
        data.add(item5);

        adapter.setData(data);

        if (data != null && data.size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
        } else {
            recyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        }
    }
}
